package actividad6;
import java.util.*;
/**
 *
 * @author victor_brito
 */
public class Operacion4 {
      private Actividad6 ultimodatoingresado;
    int tamaño;
    int r;
    String lista=" ";
    
    public Operacion4(){
        ultimodatoingresado=null;
        tamaño=0;
    }
    
    public int TamanoPila(){
        return tamaño;
    }
    
    //ingresar un dato en la pila
    public void ponerdato(int Dato){
        Actividad6 nuevo=new Actividad6(Dato);
        nuevo.siguiente=ultimodatoingresado;
        ultimodatoingresado=nuevo;
        tamaño++; 
    }
    
    //conocer el ultimo dato
     public int ultimodato(){
        return ultimodatoingresado.informacion;
    }
     
     //eliminar el ultimo dato ingresado
    public int eliminardato(){
        int cancelar=ultimodatoingresado.informacion;
        ultimodatoingresado=ultimodatoingresado.siguiente;
        tamaño--;
        return cancelar;
    }
    
    //saber si la pila esta vacia
    public boolean PilaVacia(){
        return ultimodatoingresado == null;
    }
    //mostrar la pila
    public void mostrardatos(){
        Actividad6 recorrido = ultimodatoingresado;
        
        while(recorrido != null){
            lista += recorrido.informacion + "\n";
            recorrido = recorrido.siguiente;
            
        }
        System.out.println("Contenido de la Pila"+"\n"
                +lista);
        
    }    
   
    
    public static void main(String[] args) {
      int opcion = 0, n = 0;
        Operacion4 pila = new Operacion4();
        
        do{
            try{
                Scanner leer= new Scanner(System.in);
                System.out.println("=====Botones========\n"
                        + "[1.-Insertar un dato en la pila\n"
                        
                        + "[2.-sumar dato\n"
                        
                        + "[3.-restar dato\n"
                        
                        + "[4.-cancelar el ulitmo dato ingresado\n"
                      
                        + "[5.-ver la pila\n"
                        
                        + "[6.-Terminar\n"
                        + "================");
                
                System.out.println("Elige un numero entre el  0 y 7");
                opcion=leer.nextInt();
                
                switch (opcion) {
                    case 1:
                        System.out.println("ingrese un dato a guardar en la pila");
                        n=leer.nextInt();
                        pila.ponerdato(n);
                        break;
                    
                    case 2:
                        int x;
                        int y;
                        if(!pila.PilaVacia()){
                            x=pila.ultimodato();
                            
                            System.out.println("ultimo dato + ");
                                    n=leer.nextInt();
                            
                                y= x + n;
                                
                                System.out.println("= "+ y);
                            pila.ponerdato(y);
                        }
                        else {
                            System.out.println("La pila esta vacia");
                        }
                        break;
                        
                    case 3: 
                        int o;
                        int p;
                        if(!pila.PilaVacia()){
                            o=pila.ultimodato();
                            
                            System.out.println("ultimo dato - ");
                                    n=leer.nextInt();
                            
                                p= o - n;
                                
                                System.out.println("= "+p);
                            pila.ponerdato(p);
                        }
                        else {
                            System.out.println("La pila esta vacia");
                        }
                        break;   
                    
                    case 4:
                        if(!pila.PilaVacia()){
                            System.out.println("Se ha eliminado el dato en la pila" + pila.eliminardato());
                        }else {
                            System.out.println("La pila esta vacia");
                        }
                        break;
                    
                    case 5:
                        pila.mostrardatos();
                        break;
                    
                    case 6:
                        opcion = 6;
                        break;
                        
                    default:
                        System.out.println("Pila terminada");
                        break;     
                }
            }catch (NumberFormatException e) {
                
            }
        }while (opcion != 6);
    }

    
}
